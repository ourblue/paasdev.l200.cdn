﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaaSDev.L200.CDN.Models;


namespace PaaSDev.L200.CDN.Controllers
{
    public class EHController : Controller
    {
        public IActionResult Index()
        {
            var actionName = CheckLabID(string.Empty);
            return RedirectToAction(actionName);
        }
        private string CheckLabID(string id)
        {
            string labID = Environment.GetEnvironmentVariable("L200_LABID");

            switch (labID)
            {
                case LabModel.EH_1:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabOne";
                    }
                case LabModel.EH_2:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabTwo";
                    }
                case LabModel.EH_3:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabThree";
                    }
                case LabModel.EH_4:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabFour";
                    }
                default:
                    {
                        return "LabZero";
                    }
            }
        }
        public IActionResult LabZero()
        {
            return RedirectToAction("LabZero", "Home");
        }
        public IActionResult LabOne()
        {
            var actionName = CheckLabID(LabModel.EH_1);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }

           string ehConnectionString = Environment.GetEnvironmentVariable("L200_EVENTHUB_CONNECTIONSTRING");
           string ehName = Environment.GetEnvironmentVariable("L200_EVENTHUB_NAME");

            ViewBag.ehConnectionString = ehConnectionString;
            ViewBag.ehName = ehName;

            return View();
        }
        public IActionResult LabTwo()
        {
            var actionName = CheckLabID(LabModel.EH_2);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            string ehConnectionString = Environment.GetEnvironmentVariable("L200_EVENTHUB_CONNECTIONSTRING");
            string ehName = Environment.GetEnvironmentVariable("L200_EVENTHUB_NAME");

            ViewBag.ehConnectionString = ehConnectionString;
            ViewBag.ehName = ehName;
            return View();
        }
        public IActionResult LabThree()
        {
            var actionName = CheckLabID(LabModel.EH_3);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            string ehConnectionString = Environment.GetEnvironmentVariable("L200_EVENTHUB_CONNECTIONSTRING");
            string ehName = Environment.GetEnvironmentVariable("L200_EVENTHUB_NAME");

            ViewBag.ehConnectionString = ehConnectionString;
            ViewBag.ehName = ehName;
            
            return View();
        }
        public IActionResult LabFour()
        {
            var actionName = CheckLabID(LabModel.EH_4);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            string ehConnectionString = Environment.GetEnvironmentVariable("L200_EVENTHUB_CONNECTIONSTRING");
            string ehName = Environment.GetEnvironmentVariable("L200_EVENTHUB_NAME");

            ViewBag.ehConnectionString = ehConnectionString;
            ViewBag.ehName = ehName;
            return View();
        }
    }
}