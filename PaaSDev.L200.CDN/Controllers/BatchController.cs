﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaaSDev.L200.CDN.Models;

namespace PaaSDev.L200.CDN.Controllers
{
    public class BatchController : Controller
    {
        public IActionResult Index()
        {
            var actionName = CheckLabID(string.Empty);
            return RedirectToAction(actionName);
        }
        private string CheckLabID(string id)
        {
            string labID = Environment.GetEnvironmentVariable("L200_LABID");

            switch (labID)
            {
                case LabModel.BATCH_1:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabOne";
                    }
                case LabModel.BATCH_2:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabTwo";
                    }
                case LabModel.BATCH_3:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabThree";
                    }
                case LabModel.BATCH_4:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabFour";
                    }
                default:
                    {
                        return "LabZero";
                    }
            }
        }
        public IActionResult LabZero()
        {
            return RedirectToAction("LabZero", "Home");
        }


        public IActionResult LabOne()
        {
            var actionName = CheckLabID(LabModel.BATCH_1);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            string batchUrl = Environment.GetEnvironmentVariable("L200_BATCH_URL");
            string batchKey = Environment.GetEnvironmentVariable("L200_BATCH_KEY");
            string batchName = Environment.GetEnvironmentVariable("L200_BATCH_NAME");
            string storageKey = Environment.GetEnvironmentVariable("L200_STORAGE_KEY");
            string storageName = Environment.GetEnvironmentVariable("L200_STORAGE_NAME");

            ViewBag.BatchUrl = batchUrl;
            ViewBag.BatchName = batchName;
            ViewBag.BatchKey = batchKey;
            ViewBag.StorageKey = storageKey;
            ViewBag.StorageName = storageName;

            return View();
        }
        public IActionResult LabTwo()
        {
            var actionName = CheckLabID(LabModel.BATCH_2);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            string batchUrl = Environment.GetEnvironmentVariable("L200_BATCH_URL");
            string batchKey = Environment.GetEnvironmentVariable("L200_BATCH_KEY");
            string batchName = Environment.GetEnvironmentVariable("L200_BATCH_NAME");
            string storageKey = Environment.GetEnvironmentVariable("L200_STORAGE_KEY");
            string storageName = Environment.GetEnvironmentVariable("L200_STORAGE_NAME");

            ViewBag.BatchUrl = batchUrl;
            ViewBag.BatchName = batchName;
            ViewBag.BatchKey = batchKey;
            ViewBag.StorageKey = storageKey;
            ViewBag.StorageName = storageName;
            return View();
        }
        public IActionResult LabThree()
        {
            var actionName = CheckLabID(LabModel.BATCH_3);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            string batchUrl = Environment.GetEnvironmentVariable("L200_BATCH_URL");
            string batchKey = Environment.GetEnvironmentVariable("L200_BATCH_KEY");
            string batchName = Environment.GetEnvironmentVariable("L200_BATCH_NAME");
            string storageKey = Environment.GetEnvironmentVariable("L200_STORAGE_KEY");
            string storageName = Environment.GetEnvironmentVariable("L200_STORAGE_NAME");

            ViewBag.BatchUrl = batchUrl;
            ViewBag.BatchName = batchName;
            ViewBag.BatchKey = batchKey;
            ViewBag.StorageKey = storageKey;
            ViewBag.StorageName = storageName;
            return View();
        }
        public IActionResult LabFour()
        {
            var actionName = CheckLabID(LabModel.BATCH_4);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            string batchUrl = Environment.GetEnvironmentVariable("L200_BATCH_URL");
            string batchKey = Environment.GetEnvironmentVariable("L200_BATCH_KEY");
            string batchName = Environment.GetEnvironmentVariable("L200_BATCH_NAME");
            string storageKey = Environment.GetEnvironmentVariable("L200_STORAGE_KEY");
            string storageName = Environment.GetEnvironmentVariable("L200_STORAGE_NAME");

            ViewBag.BatchUrl = batchUrl;
            ViewBag.BatchName = batchName;
            ViewBag.BatchKey = batchKey;
            ViewBag.StorageKey = storageKey;
            ViewBag.StorageName = storageName;
            return View();
        }
    }
}