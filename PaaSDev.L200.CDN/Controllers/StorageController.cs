﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaaSDev.L200.CDN.Models;

namespace PaaSDev.L200.CDN.Controllers
{
    public class StorageController : Controller
    {
        public IActionResult Index()
        {
            var actionName = CheckLabID(string.Empty);
            return RedirectToAction(actionName);
        }
        private string CheckLabID(string id)
        {
            string labID = Environment.GetEnvironmentVariable("L200_LABID");

            switch (labID)
            {
                case LabModel.STORAGE_1:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabOne";
                    }
                case LabModel.STORAGE_2:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabTwo";
                    }
                case LabModel.STORAGE_3:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabThree";
                    }
                case LabModel.STORAGE_4:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabFour";
                    }
                default:
                    {
                        return "LabZero";
                    }
            }
        }
        public IActionResult LabZero()
        {
            return RedirectToAction("LabZero", "Home");
        }
        public IActionResult LabOne()
        {
            var actionName = CheckLabID(LabModel.STORAGE_1);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            string storageConnectionString = Environment.GetEnvironmentVariable("L200_STORAGE");
            ViewBag.StorageConnectionString = storageConnectionString;
            return View();
        }
        public IActionResult LabTwo()
        {
            var actionName = CheckLabID(LabModel.STORAGE_2);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            string storageConnectionString = Environment.GetEnvironmentVariable("L200_STORAGE");
            ViewBag.StorageConnectionString = storageConnectionString;
            return View();
        }
        public IActionResult LabThree()
        {
            var actionName = CheckLabID(LabModel.STORAGE_3);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            string storageConnectionString = Environment.GetEnvironmentVariable("L200_STORAGE");
            ViewBag.StorageConnectionString = storageConnectionString;

            string storageName = Environment.GetEnvironmentVariable("L200_STORAGE_NAME");
            ViewBag.StorageName = storageName;

            string storageKey = Environment.GetEnvironmentVariable("L200_STORAGE_KEY");
            ViewBag.StorageKey = storageKey;

            
            return View();
        }
        public IActionResult LabFour()
        {
            var actionName = CheckLabID(LabModel.STORAGE_4);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            string storageConnectionString = Environment.GetEnvironmentVariable("L200_STORAGE");
            ViewBag.StorageConnectionString = storageConnectionString;
            return View();
        }
    }
}