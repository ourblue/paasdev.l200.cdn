﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaaSDev.L200.CDN.Models;

namespace PaaSDev.L200.CDN.Controllers
{
    public class SBController : Controller
    {
        public IActionResult Index()
        {
            var actionName = CheckLabID(string.Empty);
            return RedirectToAction(actionName);
        }
        private string CheckLabID(string id)
        {
            string labID = Environment.GetEnvironmentVariable("L200_LABID");

            switch (labID)
            {
                case LabModel.SB_1:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabOne";
                    }
                case LabModel.SB_2:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabTwo";
                    }
                case LabModel.SB_3:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabThree";
                    }
                case LabModel.SB_4:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabFour";
                    }
                default:
                    {
                        return "LabZero";
                    }
            }
        }
        public IActionResult LabZero()
        {
            return RedirectToAction("LabZero", "Home");
        }
        public IActionResult LabOne()
        {
            var actionName = CheckLabID(LabModel.SB_1);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }

            string sbConnectionString = Environment.GetEnvironmentVariable("L200_SERVICEBUS_CONNECTIONSTRING");
            string sbName = Environment.GetEnvironmentVariable("L200_SERVICEBUS_NAME");

            ViewBag.sbConnectionString = sbConnectionString;
            ViewBag.sbName = sbName;

            return View();
        }
        public IActionResult LabTwo()
        {
            var actionName = CheckLabID(LabModel.SB_2);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            string sbConnectionString = Environment.GetEnvironmentVariable("L200_SERVICEBUS_CONNECTIONSTRING");
            string sbName = Environment.GetEnvironmentVariable("L200_SERVICEBUS_NAME");

            ViewBag.sbConnectionString = sbConnectionString;
            ViewBag.sbName = sbName;
            return View();
        }
        public IActionResult LabThree()
        {
            var actionName = CheckLabID(LabModel.SB_3);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            string sbConnectionString = Environment.GetEnvironmentVariable("L200_SERVICEBUS_CONNECTIONSTRING");
            string sbName = Environment.GetEnvironmentVariable("L200_SERVICEBUS_NAME");

            ViewBag.sbConnectionString = sbConnectionString;
            ViewBag.sbName = sbName;

            return View();
        }
        public IActionResult LabFour()
        {
            var actionName = CheckLabID(LabModel.SB_4);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            string sbConnectionString = Environment.GetEnvironmentVariable("L200_SERVICEBUS_CONNECTIONSTRING");
            string sbName = Environment.GetEnvironmentVariable("L200_SERVICEBUS_NAME");

            ViewBag.sbConnectionString = sbConnectionString;
            ViewBag.sbName = sbName;
            return View();
        }
    }
}