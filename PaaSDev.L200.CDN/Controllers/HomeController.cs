﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using PaaSDev.L200.CDN.Models;

namespace PaaSDev.L200.CDN.Controllers
{
    public class HomeController : Controller
    {  
        public IActionResult LabZero()
        {
            return View();
        }
        public IActionResult Index()
        {
            try
            {
                string labID = Environment.GetEnvironmentVariable("L200_LABID");

                switch (labID.Split(new char[] { '_' })[0])
                {
                    default:
                        {
                            return RedirectToAction("LabZero");
                        }
                    case "CDN":
                        {
                            return RedirectToAction("Index", "CDN");
                        }
                    case "APIM":
                        {
                            return RedirectToAction("Index", "APIM");
                        }
                    case "SEARCH":
                        {
                            return RedirectToAction("Index", "Search");
                        }
                    case "STORAGE":
                        {
                            return RedirectToAction("Index", "Storage");
                        }
                    case "BATCH":
                        {
                            return RedirectToAction("Index", "Batch");
                        }
                    case "REDIS":
                        {
                            return RedirectToAction("Index", "Redis");
                        }
                    case "EH":
                        {
                            return RedirectToAction("Index", "EH");
                        }
                    case "SB":
                        {
                            return RedirectToAction("Index", "SB");
                        }
                }
            }
            catch(Exception ex)
            {
                ViewBag.Message = $"Error {ex.ToString()}";
                return RedirectToAction("LabZero");
            }
        }

        
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
