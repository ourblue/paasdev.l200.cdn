﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Search;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.Azure.Search.Models;
using Newtonsoft.Json;
using PaaSDev.L200.CDN.Models;

namespace PaaSDev.L200.CDN.Controllers
{
    public class SearchController : Controller
    {
        static string TaskErrorMessage = string.Empty;

        private Microsoft.AspNetCore.Hosting.IHostingEnvironment _env;
        public SearchController(Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            _env = env;
        }
        public IActionResult Index()
        {
            var actionName = CheckLabID(string.Empty);
            return RedirectToAction(actionName);
        }
        private string CheckLabID(string id)
        {
            string labID = Environment.GetEnvironmentVariable("L200_LABID");

            switch (labID)
            {
                case LabModel.SEARCH_1:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabOne";
                    }
                case LabModel.SEARCH_2:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabTwo";
                    }
                case LabModel.SEARCH_3:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabThree";
                    }
                case LabModel.SEARCH_4:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabFour";
                    }
                default:
                    {
                        return "LabZero";
                    }
            }
        }
        public IActionResult LabZero()
        {
            return RedirectToAction("LabZero", "Home");
        }

        public IActionResult LabTwo()
        {
            var actionName = CheckLabID(LabModel.SEARCH_2);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }

            string subscriptionID = Environment.GetEnvironmentVariable("L200_SubscriptionID");
            string resourceGroupID = Environment.GetEnvironmentVariable("L200_ResourceGroupID");
            string resourceGroupName = Environment.GetEnvironmentVariable("L200_ResourceGroupName");

#if DEBUG
            resourceGroupID = "xgjfysseihamy";
            resourceGroupName = "l200.search.2";
#endif
            Task.Factory.StartNew(() => UploadTable2Storage());

            ViewBag.SubscriptionID = subscriptionID;
            ViewBag.ResourceGroupName = resourceGroupName;
            ViewBag.StorageName = $"l200{resourceGroupID}";
            ViewBag.SearchName = $"l200{resourceGroupID}";
            //ViewBag.UrlPS1 = string.Format("{0}://{1}{2}", Request.Scheme, Request.Host, "/Search/LabTwoPS1");
            ViewBag.UrlPS1 = $"http://{Request.Host}/Search/LabTwoPS1";
            return View();
        }

        public IActionResult LabThree()
        {
            var actionName = CheckLabID(LabModel.SEARCH_3);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }

            string subscriptionID = Environment.GetEnvironmentVariable("L200_SubscriptionID");
            string resourceGroupID = Environment.GetEnvironmentVariable("L200_ResourceGroupID");
            string resourceGroupName = Environment.GetEnvironmentVariable("L200_ResourceGroupName");
            string searchName = $"l200{resourceGroupID}";

#if DEBUG
            resourceGroupID = "xgjfysseihamy";
            resourceGroupName = "l200.search.2";
#endif

            Task.Factory.StartNew(() => UploadTable2Storage());

            ViewBag.SubscriptionID = subscriptionID;
            ViewBag.ResourceGroupName = resourceGroupName;
            ViewBag.StorageName = $"l200{resourceGroupID}";
            ViewBag.SearchName = $"l200{resourceGroupID}";
            //ViewBag.UrlPS1 = string.Format("{0}://{1}{2}", Request.Scheme, Request.Host, "/Search/LabThreePS1");
            ViewBag.UrlPS1 = $"http://{Request.Host}/Search/LabThreePS1";

            return View();
        }

        [Produces("application/ps1")]
        public IActionResult LabTwoPS1()
        {
            string resourceGroupID = Environment.GetEnvironmentVariable("L200_ResourceGroupID");
            string storageConnectionString = Environment.GetEnvironmentVariable("L200_STORAGE");
            string searchKey = Environment.GetEnvironmentVariable("L200_SEARCH_KEY");
            string searchName = $"l200{resourceGroupID}";

            ViewBag.SearchKey = searchKey;
            ViewBag.SearchName = searchName;
            ViewBag.StorageConnectionString = storageConnectionString;



#if DEBUG
            ViewBag.SearchKey = "4BD11018B4C9D6C6F1C9E81FCFA22704";
            ViewBag.SearchName = "l200vnnrlc6bsl2qm";
            ViewBag.StorageConnectionString = "DefaultEndpointsProtocol=https;AccountName=l200vnnrlc6bsl2qm;AccountKey=OexIklR/rfgCIrVDIATQIeeFIz0TcZPIuWr/MdlblI5ce4cy6JvezmlSa6B8XR97vNM/3RipQ0oQPS6byNFOTw==;EndpointSuffix=core.windows.net";
#endif
            return View();
        }

        [Produces("application/ps1")]
        public IActionResult LabThreePS1()
        {
            string subscriptionID = Environment.GetEnvironmentVariable("L200_SubscriptionID");
            string resourceGroupID = Environment.GetEnvironmentVariable("L200_ResourceGroupID");
            string storageConnectionString = Environment.GetEnvironmentVariable("L200_STORAGE");
            string searchKey = Environment.GetEnvironmentVariable("L200_SEARCH_KEY");
            string searchName = $"l200{resourceGroupID}";

            ViewBag.SearchKey = searchKey;
            ViewBag.SearchName = searchName;
            ViewBag.StorageConnectionString = storageConnectionString;
            ViewBag.TinyGetPath = string.Format("{0}://{1}{2}", Request.Scheme, Request.Host, "/content/tinyget.bin");
            //ViewBag.TinyGetPath = $"http://{Request.Host}/content/tinyget.bin";


#if DEBUG
            ViewBag.SearchKey = "39A7A3CCA2B41615CEC6AC1A077479EE";
            ViewBag.SearchName = "l200xgjfysseiham";
            ViewBag.StorageConnectionString = "DefaultEndpointsProtocol=https;AccountName=l200xgjfysseihamy;AccountKey=g46P7Hk4ZlrjgnnZAWghZJfkk56L5SVR9HwnOTjhPl/yu/7WNBOh22w64yDGpJ/9tohUMA9cJbhxnBzwTm+Emw==;EndpointSuffix=core.windows.net";
#endif
            return View();
        }


        public async Task<IActionResult> LabOne()
        {
            var actionName = CheckLabID(LabModel.SEARCH_1);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }

            //await UploadFiles2Storage();
            Task.Factory.StartNew(() => UploadTable2Storage());

            string subscriptionID = Environment.GetEnvironmentVariable("L200_SubscriptionID");
            string resourceGroupID = Environment.GetEnvironmentVariable("L200_ResourceGroupID");
            string resourceGroupName = Environment.GetEnvironmentVariable("L200_ResourceGroupName");

            ViewBag.SubscriptionID = subscriptionID;
            ViewBag.StorageName = $"l200{resourceGroupID}";
            ViewBag.ResourceGroupName = resourceGroupName;

            ViewBag.Message = TaskErrorMessage;

            return View();
        }

        private async Task UploadTable2Storage()
        {
            try
            {
                CloudStorageAccount storageAccount = null;
                string storageConnectionString = Environment.GetEnvironmentVariable("L200_STORAGE");
#if DEBUG
                storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=l2003ktc5akbt4lq2;AccountKey=qMJmzOzzgYtSirCD+0NQPDFK9zc/sV1wYNzm/fObSaDQELo2GRMjdEnhtYoMqJ8+ObFZEqHB9uZZdQQvYEw6Ag==;EndpointSuffix=core.windows.net";
#endif

                // Check whether the connection string can be parsed.
                if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
                {
                    try
                    {
                        //   create a JSON file with 100,000 records
                        //CreateJsonFile();

                        // Create the table client.
                        CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

                        // Create the CloudTable object that represents the "Employee" table.
                        CloudTable table = tableClient.GetTableReference("Employee");

                        // Create the table if it doesn't exist.
                        bool bNew = await table.CreateIfNotExistsAsync();

                        // this is not new 
                        if (!bNew)
                        {
                            TaskErrorMessage = "Storage setup done.";
                            return;
                        }

                        // Create the batch operation.
                        TableBatchOperation batchOperation = new TableBatchOperation();



                        Random r = new Random();

                        string[] designaations = new string[] { "CEO", "President", "Vice President", "Delivery Manager", "Software Engineer", "Trainee" };
                        string[] interests = new string[] { "Amateur Astronomy", "Ghost Hunting", "Skiing", "Photography", "Toy Collecting",
                "Jet Engines", "Gardening", "Listening to music", "Powerboking", "BoardGames",
                "Socializing with friends/neighbors", "Pipe Smoking", "Parachuting","Car Racing","Dolls",
                "Woodworking","Tennis","Wine Making","Collecting RPM Records", "Football"};

                        int[] ShuffleList = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 };

                        StringBuilder sbInterest = new StringBuilder();
                        int idesignaation = 0, ninterest = 0;

                        DateTime start = new DateTime(1975, 1, 1);
                        int range = (DateTime.Today - start).Days;



                        for (int pk = 0; pk < 1000; pk++)
                        {
                            batchOperation.Clear();

                            for (int rk = 0; rk < 100; rk++)
                            {
                                idesignaation = r.Next(0, 5);
                                ninterest = r.Next(3, 10);

                                //
                                for (int n = ShuffleList.Length - 1; n > 0; --n)
                                {
                                    int k = r.Next(n + 1);
                                    int temp = ShuffleList[n];
                                    ShuffleList[n] = ShuffleList[k];
                                    ShuffleList[k] = temp;
                                }

                                sbInterest.Clear();
                                sbInterest.Append(interests[r.Next(0, 19)]);
                                for (int j = 0; j < ninterest; j++)
                                {
                                    sbInterest.Append($",{interests[ShuffleList[j]]}");
                                }

                                //batchOperation.Insert
                                TableOperation insertOperation = TableOperation.InsertOrMerge
                                    (new EmployeeModel()
                                    {
                                        PartitionKey = $"PKey_{pk}",
                                        RowKey = $"RKey_{rk}",
                                        Timestamp = DateTime.Now,
                                        EmpID = $"{(pk * 100) + rk}",
                                        Address = $"Address_{rk}",
                                        Age = r.Next(20, 65),
                                        DateOfJoining = start.AddDays(r.Next(range)),
                                        Designation = designaations[idesignaation],
                                        Gender = r.Next(0, 100) > 50 ? "Male" : "Female",
                                        Interests = sbInterest.ToString(),
                                        MaritalStatus = r.Next(0, 100) > 50 ? "Married" : "Unmarried",
                                        Salary = (r.Next(40, 400) * 1000)
                                    });

                                Console.WriteLine($"PartitionKey : {pk} \t Rowkey : {rk}");
                                batchOperation.Add(insertOperation);
                                // Execute the insert operation.
                                //await table.ExecuteAsync(insertOperation);
                            }
                            Console.WriteLine($"Pk : {pk}");
                            // Execute the batch operation.
                            await table.ExecuteBatchAsync(batchOperation);
                        }
                    }
                    catch (Exception ex)
                    {
                        TaskErrorMessage = ex.ToString();
                    }
                }
                else
                {
                    TaskErrorMessage = "storage connection string is missing.";
                }
            }
            catch (Exception ex)
            {
                TaskErrorMessage = ex.ToString();
            }
        }

    }
}