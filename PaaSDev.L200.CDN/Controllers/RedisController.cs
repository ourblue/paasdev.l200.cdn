﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaaSDev.L200.CDN.Models;

namespace PaaSDev.L200.CDN.Controllers
{
    public class RedisController : Controller
    {
        public IActionResult Index()
        {
            var actionName = CheckLabID(string.Empty);
            return RedirectToAction(actionName);
        }
        private string CheckLabID(string id)
        {
            string labID = Environment.GetEnvironmentVariable("L200_LABID");

            switch (labID)
            {
                case LabModel.REDIS_1:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabOne";
                    }
                case LabModel.REDIS_2:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabTwo";
                    }
                case LabModel.REDIS_3:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabThree";
                    }
                case LabModel.REDIS_4:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabFour";
                    }
                default:
                    {
                        return "LabZero";
                    }
            }
        }
        public IActionResult LabZero()
        {
            return RedirectToAction("LabZero", "Home");
        }
        public IActionResult LabOne()
        {
            var actionName = CheckLabID(LabModel.REDIS_1);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }

            string redis = Environment.GetEnvironmentVariable("L200_REDIS_CONNECTIONSTRING");

            ViewBag.REDIS = redis;


            return View();
        }
        
        public IActionResult LabTwo()
        {
            var actionName = CheckLabID(LabModel.REDIS_2);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }

            string redis = Environment.GetEnvironmentVariable("L200_REDIS_CONNECTIONSTRING");

            ViewBag.REDIS = redis;
            return View();
        }

        public IActionResult LabThree()
        {
            var actionName = CheckLabID(LabModel.REDIS_3);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            string redis = Environment.GetEnvironmentVariable("L200_REDIS_CONNECTIONSTRING");

            ViewBag.REDIS = redis;
            return View();
        }

        public IActionResult LabFour()
        {
            var actionName = CheckLabID(LabModel.REDIS_4);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }

            string redis = Environment.GetEnvironmentVariable("L200_REDIS_CONNECTIONSTRING");

            ViewBag.REDIS = redis;
            return View();
        }
    }
}