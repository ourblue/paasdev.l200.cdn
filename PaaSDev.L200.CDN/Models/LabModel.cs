﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaaSDev.L200.CDN.Models
{
    public class LabModel
    {
        public const string CDN_1 = "CDN_1";
        public const string CDN_2 = "CDN_2";
        public const string CDN_3 = "CDN_3";
        public const string CDN_4 = "CDN_4";

        public const string APIM_1 = "APIM_1";
        public const string APIM_2 = "APIM_2";
        public const string APIM_3 = "APIM_3";
        public const string APIM_4 = "APIM_4";

        public const string SEARCH_1 = "SEARCH_1";
        public const string SEARCH_2 = "SEARCH_2";
        public const string SEARCH_3 = "SEARCH_3";
        public const string SEARCH_4 = "SEARCH_4";

        public const string STORAGE_1 = "STORAGE_1";
        public const string STORAGE_2 = "STORAGE_2";
        public const string STORAGE_3 = "STORAGE_3";
        public const string STORAGE_4 = "STORAGE_4";

        public const string BATCH_1 = "BATCH_1";
        public const string BATCH_2 = "BATCH_2";
        public const string BATCH_3 = "BATCH_3";
        public const string BATCH_4 = "BATCH_4";

        public const string REDIS_1 = "REDIS_1";
        public const string REDIS_2 = "REDIS_2";
        public const string REDIS_3 = "REDIS_3";
        public const string REDIS_4 = "REDIS_4";

        public const string EH_1 = "EH_1";
        public const string EH_2 = "EH_2";
        public const string EH_3 = "EH_3";
        public const string EH_4 = "EH_4";

        public const string SB_1 = "SB_1";
        public const string SB_2 = "SB_2";
        public const string SB_3 = "SB_3";
        public const string SB_4 = "SB_4";


    }
}
