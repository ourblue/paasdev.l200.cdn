﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaaSDev.L200.CDN.Models
{
    public class EmployeeModel : TableEntity
    {
        //public string PartitionKey { get; set; }
        //public string RowKey { get; set; }
        //public DateTime Timestamp { get; set; }
        public string EmpID { get; set; }
        public string Address { get; set; }
        public int Age { get; set; }
        public DateTime DateOfJoining { get; set; }
        public string Designation { get; set; }
        public string Gender { get; set; }
        public string Interests { get; set; }
        public string MaritalStatus { get; set; }
        public int Salary { get; set; }
    }
}
